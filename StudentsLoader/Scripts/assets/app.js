/* ====== scripts\app.js ===-_-=== */
var app = angular.module('StudentLoader',[
  'ngAnimate',
  'toastr',
  'angularFileUpload'
], function($httpProvider){

	// Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
//remove the limitation for cross domain access of javascript domains
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

/**
 * The workhorse; converts an object to x-www-form-urlencoded serialization.
 * @param {Object} obj
 * @return {String}
 */
  var param = function(obj) {

    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

// Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];

});

app.value('_', '_');
app.value('$', '$');
app.value('moment', 'moment');
/* ====== scripts\run.js ===-_-=== */
app.run([
    '_config',
    '_regExp',
    '$rootScope',
    '$timeout',
    '$window',
function(_config, _regExp, $rootScope, $timeout, $window) {
    _.extend($rootScope, _regExp);
    _.extend($rootScope, _config);

    $rootScope.redirecTo = function(urlRedirection, timeWaiting){
    	timeWaiting = (timeWaiting)? _config.defaultTimeout : 0;
    	$timeout(function() {
    		$window.location.href = _config.baseUrl + urlRedirection;
    	}, timeWaiting);
    };

    $('span').tooltip();
}]);
/* ====== scripts\config.js ===-_-=== */
app.config(function(){

});
/* ====== scripts\constants.js ===-_-=== */
app.constant('_config', {
	'appRefenrece': 'StudentLoader',
	'defaultTimeout': 3000,
	'baseUrl': "http://localhost:3000/",
	'requestTimeout': 3000,
	'genericErrorMessage':  "Internal Server Error has ocurr, try agein",
	'excelStudentLoaderFile': "Content/Formats/studentLoaderFormat_v1.xlsx"
});

app.constant('_apiEndpointsList', {
	'signin': 'Login/SignIn',
	'createStudent': 'Student/AddStudent',
	'updateStudent': 'Student/UpdateStudent',
	'deleteStudent': 'Student/DeleteStudent',
	'importStudents': 'Student/ImportStudents',
	'createGroup': 'Group/AddGroup',
	'deleteGroup': 'Group/DeleteGroup',
	'updateGroup': 'Group/UpdateStudentsGroup'
});

app.constant('_regExp', {
	'onlyNumbersPattern': /^[0-9.]+$/,
	'onlyAlphabethPattern': /^[a-z _-]+$/,
	'emailPattern': /^[a-z]+[a-z0-9+._]+@[a-z]+\.[a-z.]{2,5}$/
});
/* ====== scripts\controllers\groupDetail_controller.js ===-_-=== */
app.controller('groupDetailCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {

	$scope.notifyAllStudents = false;

	$scope.initializeActions = function(studentIdentifier, hasEmail, score) {
		$scope["studentSendEmailDisabled_" + studentIdentifier] = (_.isEmpty(hasEmail))? true : false;
		$scope["studentSendEmailChecked_" + studentIdentifier] = false;
		$scope["studentScore_" + studentIdentifier] = score;
	}

	$scope.toggleAll = function() {
		var $rowItems = $(".student-row"),
			studentIdentifier;

		if($scope.notifyAllStudents) {
			_.each($rowItems, function(studentItem) {
				studentIdentifier = $(studentItem).data('identifier');
				if(!$scope["studentSendEmailDisabled_" + studentIdentifier]) {
					$scope["studentSendEmailChecked_" + studentIdentifier] = true;
				}
			});
		} else {
			_.each($rowItems, function(studentItem) {
				studentIdentifier = $(studentItem).data('identifier');
				$scope["studentSendEmailChecked_" + studentIdentifier] = false;
			});
		}
	};

	$scope.goGroupList = function(wait) {
		$rootScope.redirecTo('Group/Index', wait);
	}

	$scope.updateStudentsNotes = function() {
		var $rowItems = $(".student-row"),
			student,
			studentIdentifier;

		$scope.studentsData = [];

		_.each($rowItems, function(studentItem) {
			studentIdentifier = $(studentItem).data('identifier');
			student = {
				"studentIdentifier": studentIdentifier,
				"score": $scope['studentScore_' + studentIdentifier] + "",
				"sendEmail": $scope["studentSendEmailChecked_" + studentIdentifier]
			};
			$scope.studentsData.push(student);
		});

		var requestData = {
			"studentsScoreData": JSON.stringify($scope.studentsData)
		};

		APIRequestService.updateGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$scope.goGroupList(true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};

}]);
/* ====== scripts\controllers\groupIndex_controller.js ===-_-=== */
app.controller('groupIndexCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {


	$scope.detailGroup = function(groupIdentifier) {
		$rootScope.redirecTo('Group/Detail/' + groupIdentifier, false);
	};

	$scope.removeGroup = function(groupIdentifier) {
		$scope["showWarning_" + groupIdentifier] = true;
	};

	$scope.cancelRemoveActionFor = function(groupIdentifier) {
		$scope["showWarning_" + groupIdentifier] = false;
	};

	$scope.confirmRemoveActionFor = function(groupIdentifier) {
		var requestData = {
			"groupIdentifier": groupIdentifier
		};

		APIRequestService.deleteGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Group/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};

	$scope.createGroup = function() {
		var requestData= {};

		APIRequestService.createGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Group/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	}
}]);
/* ====== scripts\controllers\login_controller.js ===-_-=== */
app.controller('loginCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr){
	var formFields = ['username', 'password'];

	$scope.singUp = function(formData){
		if(formData.$valid){
			var requestData = {
				'username': $scope.formUsername,
				'password': $scope.formPassword
			};

			APIRequestService.singin(requestData, function(error, response){
				if(!error && response &&  response.data && response.data.reason == 'success'){
					toastr.success(response.data.message, "Success");
					$rootScope.redirecTo('Student/Index', true);
				} else if (!error && response.data.reason == 'error') {
					toastr.error(response.data.message, "Error");
				} else {
					toastr.error($rootScope.genericErrorMessage, "Error");
				}
			});
		}
	};

	$scope.showFieldErrors = function(formData, errors){
		_.each(formFields, function(field){
			formData[field].$error.invalid = true;
			formData[field].$error_message = errors[field];
		});
	};

}]);
/* ====== scripts\controllers\studentAdd_controller.js ===-_-=== */
app.controller('studentAddCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr){

	$scope.goStudentsList = function(wait) {
		$rootScope.redirecTo('Student/Index', wait);
	}

	$scope.createStudent = function(formData) {
		if(formData.$valid) {
			var requestData = {
				"code": $scope.studentCode,
				"name": $scope.studentName,
				"email": $scope.studentEmail,
				"group": $scope.studentGroup
			};

			APIRequestService.createStudent(requestData, function(error, response){
				if(!error && response &&  response.data && response.data.reason == 'success'){
					toastr.success(response.data.message, "Success");
					$scope.goStudentsList(true);
				} else if (!error && response.data.reason == 'error') {
					toastr.error(response.data.message, "Error");
				} else {
					toastr.error($rootScope.genericErrorMessage, "Error");
				}
			});
		}
	};

}]);
/* ====== scripts\controllers\studentEdit_controller.js ===-_-=== */
app.controller('studentEditCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr){

	$scope.goStudentsList = function(wait) {
		$rootScope.redirecTo('Student/Index', wait);
	}

	$scope.updateStudent = function(formData) {
		if(formData.$valid) {
			var requestData = {
				"studentIdentifier": $scope.studentIdentifier,
				"code": $scope.studentCode,
				"name": $scope.studentName,
				"email": $scope.studentEmail,
				"group": $scope.studentGroup
			};

			APIRequestService.updateStudent(requestData, function(error, response){
				if(!error && response &&  response.data && response.data.reason == 'success'){
					toastr.success(response.data.message, "Success");
					$scope.goStudentsList(true);
				} else if (!error && response.data.reason == 'error') {
					toastr.error(response.data.message, "Error");
				} else {
					toastr.error($rootScope.genericErrorMessage, "Error");
				}
			});
		}
	};

}]);
/* ====== scripts\controllers\studentImport_controller.js ===-_-=== */
app.controller('studentImportCtrl', [
	'_config',
	'_apiEndpointsList',
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
	'FileUploader',
function(_config, _apiEndpointsList, $rootScope, $scope, APIRequestService, toastr, FileUploader){
	$scope.uploader = new FileUploader({
        url: _apiEndpointsList.importStudents
    });

	$scope.file = {
		progressPositive: 5,
		progressNegative: 95
	};
    $scope.showFileInput = false;
    $scope.showFileDescription = false;
    $scope.disabledImportAction = true;
    $scope.showProgressbar = false;
    $scope.showskipedExcelRecords = false;

    $scope.getExcelFormat = function() {
    	window.open(_config.excelStudentLoaderFile);
    };

    $scope.triggerUploadEvent = function() {
    	$("input[type='file']").trigger('click');
    }

	$scope.goStudentsList = function(wait) {
		$rootScope.redirecTo('Student/Index', wait);
	}

	$scope.getTypeOfFile = function(fileReference) {
		var data = {
			icon: "fa-question",
			type: "Unknow"
		};

		switch(fileReference) {
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				data.type = 'Excel';
				data.icon = 'fa-file-excel-o';
				break;
			case "application/msword":
				data.type = 'Word';
				data.icon = 'fa-file-word-o';
				break;
			case "image/jpeg":
			case "image/png":
			case "image/gif":
				data.type = 'Image';
				data.icon = 'fa-file-image-o';
				break;
			case "application/pdf":
				data.type = 'Pdf';
				data.icon = 'fa-file-pdf-o';
				break;
		}

		return data;
	}

    $scope.uploader.onWhenAddingFileFailed = function(item, filter, options) {
    	$scope.showFileDescription = false;
        toastr.error("Something when wrong", "Error");
    };

    $scope.uploader.onAfterAddingFile = function(fileItem) {
    	var fileInfo = $scope.getTypeOfFile(fileItem.file.type);
    	
    	$scope.showFileDescription = true;
    	$scope.file['name'] = fileItem.file.name;
    	$scope.file['size'] = fileItem.file.size;
    	$scope.file['lastModifiedDate'] = moment(fileItem.file.lastModifiedDate.getTime()).format();
    	$scope.file['type'] = fileInfo.type;
    	$scope.file['icon'] = fileInfo.icon;
    };
    
    $scope.uploader.onBeforeUploadItem = function(item) {
    	$scope.showProgressbar = true;
        $scope.showskipedExcelRecords = false;
    };

    $scope.uploader.onProgressItem = function(fileItem, progress) {
    	$scope.file.progressPositive = progress;
    	$scope.file.progressNegative = (100 - progress);
    };

    $scope.uploader.onProgressAll = function(progress) {
    	$scope.file.progressPositive = progress;
    	$scope.file.progressNegative = (100 - progress);
    };

    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
    	if (response && response.reason === "success") {
            if (response.data) {
                $scope.showSkipedRecords(response.data);
                toastr.warning(response.message, "Warning");
                $scope.showProgressbar = false;
            } else {
        		toastr.success(response.message, "Success");
        		$scope.goStudentsList(true);
            }
    	} else if(response && response.reason === "error") {
    		$scope.showProgressbar = false;
    		toastr.error(response.message, "Error");
    	} else {
    		$scope.showProgressbar = false;
    		toastr.error("Something when wrong, try again", "Error");
    	}
    };

    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
    	console.log(response);
    	toastr.error("Something when wrong", "Error");
    };

    $scope.showSkipedRecords = function(records) {
        var $skippedList = $("#skipedExcelRecords");
        $skippedList.empty();
        _.each(records, function(value, key){
            $skippedList.append("<li>"+value+"</li>");
        });
        $scope.showskipedExcelRecords = true;
    };

}]);
/* ====== scripts\controllers\studentIndex_controller.js ===-_-=== */
app.controller('studentIndexCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {

	$scope.uploadExcelFile = function() {
		$rootScope.redirecTo('Student/Import', false);
	};

	$scope.createUser = function() {
		$rootScope.redirecTo('Student/Add', false);
	};

	$scope.editStudent = function(studentIdentifier) {
		$rootScope.redirecTo('Student/Edit/'+ studentIdentifier, false);
	};

	$scope.removeStudent = function(studentIdentifier) {
		$scope["showWarning_" + studentIdentifier] = true;
	};

	$scope.cancelRemoveActionFor = function(studentIdentifier) {
		$scope["showWarning_" + studentIdentifier] = false;
	};

	$scope.confirmRemoveActionFor = function(studentIdentifier) {
		var requestData = {
			"studentIdentifier": studentIdentifier
		};

		APIRequestService.deleteStudent(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Student/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};
}]);
/* ====== scripts\directives\adminMenu_directive.js ===-_-=== */
app.directive( 'adminMenu', function(_config, appHelper, $rootScope){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('admin-menu'),
		link: function(scope, element, attrs) {

		}
	};
});
/* ====== scripts\directives\errorFieldNotification_directive.js ===-_-=== */
app.directive( 'errorFieldNotification', function(_config, appHelper){
  return {
    restrict: 'E',
    templateUrl: appHelper.templatePath('error-field-notification'),
    scope:{
      fieldToValidate: '=fieldToValidate',
      minlength: '=minlength',
      maxlength: '=maxlength',
    }
  };
});
/* ====== scripts\directives\mainMenu_directive.js ===-_-=== */
app.directive( 'mainMenu', function(_config, appHelper, $rootScope){
	return {
		restrict: 'E',
		templateUrl: appHelper.templatePath('main-menu'),
		link: function(scope, element, attrs) {

		}
	};
});
/* ====== scripts\services\APIrequest_service.js ===-_-=== */
app.factory('APIRequestService', [
  '_config',
  '_apiEndpointsList',
  '$http',
function (_config, _apiEndpointsList, $http) {
    var APIRequestService = this;

    APIRequestService.madeRequestAction = function(method, endpointReference, requestData, addTokenToRequest, callback) {
      //define the http options
      var httpOptions = {
          method : method,
          url    : _config.baseUrl + endpointReference,
          data   : requestData,
          cache  : false,
          timeout: _config.requestTimeout
      };

      $http(httpOptions).then(function successCallback(response) {
    		callback(null, response);
      }, function errorCallback(response) {
      	callback(response.data);
      });
    };

    APIRequestService.singin = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST',_apiEndpointsList.signin, requestData,  false, callback);
    };

    APIRequestService.createStudent = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.createStudent, requestData,  false, callback);
    };

    APIRequestService.updateStudent = function (requestData, callback) {
    	APIRequestService.madeRequestAction('POST', _apiEndpointsList.updateStudent, requestData,  false, callback);
    };

    APIRequestService.deleteStudent = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.deleteStudent, requestData,  false, callback);
    };

    APIRequestService.importStudents = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.importStudents, requestData,  false, callback);
    };

    APIRequestService.createGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.createGroup, requestData,  false, callback);
    };

    APIRequestService.deleteGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.deleteGroup, requestData,  false, callback);
    };

    APIRequestService.updateGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.updateGroup, requestData,  false, callback);
    };

    return APIRequestService;
}]);
/* ====== scripts\services\appHelper.js ===-_-=== */
app.constant('appHelper', {

  templatesDir: function() {
  	return 'http://localhost:3000/Content/templates';
  },

  templatePath: function(view_name){
    return this.templatesDir() + '/' + view_name + '.html';
  }

});