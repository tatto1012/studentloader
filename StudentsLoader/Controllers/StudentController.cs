﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsLoader.Models;
using StudentsLoader.Helpers;
using Newtonsoft.Json.Linq;

namespace StudentsLoader.Controllers
{
    public class StudentController : Controller
    {
        public ActionResult Index()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            students students_model = new students();
            var studentList = students_model.getAllStudents();

            return View(studentList);
        }

        public ActionResult Edit(int id)
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            students students_model = new students();
            groups groups_model = new groups();

            ViewData["groups"] = groups_model.getAllGroups().ToList();
            ViewData["studentData"] = students_model.getStudentById(id);

            return View();
        }

        public ActionResult Add()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var studentCode = new CodeGenerator();
            groups groups_model = new groups();

            ViewData["groups"] = groups_model.getAllGroups().ToList();
            ViewData["studentCode"] = studentCode.getNewCode();

            return View();
        }

        public ActionResult Import()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View();
        }

        [HttpPost]
        public ActionResult UpdateStudent()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var studentIdentifier = Int32.Parse(Request["studentIdentifier"]);
            var code = double.Parse(Request["code"]);
            var name = Request["name"];
            var email = Request["email"];
            var group = Int32.Parse(Request["group"]);

            students students_model = new students();
            students_model.udateStudent(studentIdentifier, code, name, email, group, "0", 0);

            var postParams = new JObject();
            postParams["code"] = code;
            postParams["name"] = name;
            postParams["email"] = email;
            postParams["group"] = group;

            var restData = new JsonResponse();
            restData.setJsonResponse("success", true, "The student has been updated successfully");
            restData.setDataResponse(postParams);

            return Content(restData.getStringResponse(), "application/json");
        }

        [HttpPost]
        public ActionResult DeleteStudent()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var studentIdentifier = Int32.Parse(Request["studentIdentifier"]);

            var restData = new JsonResponse();
            restData.setJsonResponse("success", true, "The student has been removed successfully");

            students students_model = new students();
            students_model.removeStudent(studentIdentifier);

            return Content(restData.getStringResponse(), "application/json");
        }

        [HttpPost]
        public ActionResult AddStudent()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var code = double.Parse(Request["code"]);
            var name = Request["name"];
            var email = Request["email"];
            var group = Int32.Parse(Request["group"]);

            students students_model = new students();

            var studentExists = students_model.getStudentByName(name);
            var restData = new JsonResponse();

            if (studentExists == null)
            {
                students_model.addStudent(code, name, email, group);

                var postParams = new JObject();
                postParams["code"] = code;
                postParams["name"] = name;
                postParams["email"] = email;
                postParams["group"] = group;

                restData.setJsonResponse("success", true, "The student has been created successfully");
                restData.setDataResponse(postParams);
            }
            else
            {
                restData.setJsonResponse("error", false, "The student already exist!");
            }

            return Content(restData.getStringResponse(), "application/json");
        }

        [HttpPost]
        public ActionResult ImportStudents(HttpPostedFileBase file)
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var restData = new JsonResponse();

            if (Request.Files["file"].ContentLength > 0)
            {
                var excelHelper = new ExcelHelper(Request.Files["file"], Server.MapPath("~/Content/tmp/"));
                string resultProcessMessage = "Excel with student list has been uploaded successfully";
                if (excelHelper.HasValidEstension())
                {
                    JObject excelResponse = excelHelper.getProcessResponse().getJsonResult();
                    string reason = excelResponse["reason"].ToString();

                    if (reason == "error")
                    {
                        resultProcessMessage = excelResponse["message"].ToString();
                        JObject dataErrors = (JObject) excelResponse["data"];
                        restData.setDataResponse(dataErrors);
                    }

                    restData.setJsonResponse("success", true, resultProcessMessage);
                }
                else
                {
                    restData.setJsonResponse("error", false, "wrong file extension, only are allowed Excel files (.xls, .xlsx)");
                }
            }
            else
            {
                restData.setJsonResponse("error", false, "File not received, plase ensure that you are uploading the file");
            }

            return Content(restData.getStringResponse(), "application/json");
        }

    }
}