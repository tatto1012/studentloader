﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsLoader.Helpers;
using StudentsLoader.Models;

namespace StudentsLoader.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            var username = Request["username"];
            var password = Request["password"];

            users user_model= new users();
            var userData = user_model.getUserLogin(username, password);
            var restData = new JsonResponse();

            if (userData != null)
            {
                Session["userData"] = userData;
                restData.setJsonResponse("success", true, "You are login successfully");
            }
            else
            {
                Session["userData"] = null;
                restData.setJsonResponse("error", false, "Your credential are wrong try again");
            }

            return Content(restData.getStringResponse(), "application/json");
        }

        public ActionResult Logout()
        {
            Session["userData"] = null;
            return RedirectToAction("Index", "Login");
        }
    }
}