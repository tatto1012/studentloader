﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsLoader.Models;
using StudentsLoader.Helpers;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace StudentsLoader.Controllers
{
    public class GroupController : Controller
    {

        public ActionResult Index()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            groups groups_model = new groups();
            var groupList = groups_model.getAllGroups();

            return View(groupList);
        }

        public ActionResult Detail(int id)
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            groups groups_model = new groups();
            students students_model = new students();
            groups groupData = groups_model.getGroupById(id);

            ViewData["groupData"] = groupData;
            ViewData["students"] = students_model.getAllStudentsByGroup(groupData.code);

            return View();
        }

        [HttpPost]
        public ActionResult AddGroup()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            groups groups_model = new groups();
            var groupList = groups_model.getAllGroups().ToList();
            var restData = new JsonResponse();
            var lastItem = 800;

            if (groupList != null)
            {
                foreach (groups groupItem in groupList)
                {
                    lastItem = (lastItem + groupItem.id);
                }
            }

            groups_model.addGroup(lastItem);
            restData.setJsonResponse("success", true, "The group has been created successfully");

            return Content(restData.getStringResponse(), "application/json");
        }

        [HttpPost]
        public ActionResult DeleteGroup()
        {
            if (Session["userData"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var groupIdentifier = Int32.Parse(Request["groupIdentifier"]);

            var restData = new JsonResponse();
            restData.setJsonResponse("success", true, "The student has been removed successfully");

            groups groups_model = new groups();
            groups_model.removeGroup(groupIdentifier);

            return Content(restData.getStringResponse(), "application/json");
        }

        [HttpPost]
        public ActionResult UpdateStudentsGroup()
        {
            var restData = new JsonResponse();
            string studentsPostData = Request["studentsScoreData"];

            if (studentsPostData != null)
            {
                List<GroupStudentsScoreHelper> postStudentData;
                students students_model = new students();
                postStudentData =(List<GroupStudentsScoreHelper>) Newtonsoft.Json.JsonConvert.DeserializeObject(studentsPostData, typeof(List<GroupStudentsScoreHelper>));

                foreach(GroupStudentsScoreHelper studentScore in postStudentData)
                {
                    if (studentScore.score != "0")
                    {
                        var studentData = students_model.getStudentById(studentScore.studentIdentifier);

                        if (studentScore.sendEmail)
                        {
                            MandrillHelper md = new MandrillHelper();
                            md.sendMessage(studentData.email, studentData.group.ToString(), studentData.name, studentScore.score);
                            studentData.notified = Convert.ToInt16(studentScore.sendEmail);
                        }

                        studentData.score = studentScore.score;
                        studentData.udateStudent(
                            studentScore.studentIdentifier,
                            studentData.code,
                            studentData.name,
                            studentData.email,
                            studentData.group,
                            studentData.score,
                            studentData.notified
                        );
                    }
                }

                restData.setJsonResponse("success", true, "The group has been updated succesfully");
            }
            else
            {
                restData.setJsonResponse("error", true, "cannot process student group data");
            }

            return Content(restData.getStringResponse(), "application/json");
        }
    }
}