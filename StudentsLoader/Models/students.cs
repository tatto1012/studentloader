namespace StudentsLoader.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using StudentsLoader.Helpers;
    using System.Linq;


    [Table("projectsecondbreak.students")]
    public partial class students
    {
        private Models.psbDBModel db;
        private DateHelper dh;

        public int id { get; set; }

        public double code { get; set; }

        [StringLength(150)]
        public string name { get; set; }

        [StringLength(150)]
        public string email { get; set; }

        public int group { get; set; }

        public string score { get; set; }

        public int notified { get; set; }

        public int? created_at { get; set; }

        public int? modified_at { get; set; }

        public students()
        {
            Models.psbDBModel databaseInstance = new Models.psbDBModel();
            this.db = databaseInstance;
            this.dh = new DateHelper();
        }

        public List<students> getAllStudents()
        {
            return db.students.Where(a => a.id != 0).ToList();
        }

        public Models.students getStudentById(int studentIdentifier)
        {
            return this.db.students.Find(studentIdentifier);
        }

        public void addStudent(double codeValue, string nameValue, string emailValue, int groupValue) {

            this.db.students.Add(new Models.students
            {
                code = codeValue,
                name = nameValue,
                email = emailValue,
                group = groupValue,
                notified = 0,
                score = "0",
                created_at = this.dh.getCurrentTime()
            });

            this.db.SaveChanges();
        }

        public void udateStudent(int studentIdentifier, double codeValue, string nameValue, string emailValue, int groupValue, string scoreValue, int notifiedValue)
        {
            var studentData = this.db.students.Find(studentIdentifier);

            if (studentData != null)
            {
                studentData.code = codeValue;
                studentData.name = nameValue;
                studentData.email = emailValue;
                studentData.group = groupValue;

                if (scoreValue != null)
                {
                    studentData.score = scoreValue;
                }

                if (notifiedValue != 0)
                {
                    studentData.notified = notifiedValue;
                }

                studentData.modified_at = dh.getCurrentTime();

                this.db.SaveChanges();
            }
        }

        public void removeStudent(int studentIdentifier)
        {
            var studentData = this.db.students.Find(studentIdentifier);

            if (studentData != null)
            {
                this.db.students.Remove(studentData);
                this.db.SaveChanges();
            }
        }

        public students getStudentByName(string nameValue)
        {
            var studentData = this.db.students.Where(a => a.name == nameValue).FirstOrDefault();
            return studentData;
        }

        public List<students> getAllStudentsByGroup(int? groupCode)
        {
            var studentsData = this.db.students.Where(a => a.group == groupCode).ToList();
            return studentsData;
        }
    }
}
