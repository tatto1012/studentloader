namespace StudentsLoader.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using StudentsLoader.Helpers;
    using System.Linq;

    [Table("projectsecondbreak.users")]
    public partial class users
    {
        private Models.psbDBModel db;
        private DateHelper dh;

        public int id { get; set; }

        [StringLength(150)]
        public string name { get; set; }

        [StringLength(150)]
        public string username { get; set; }

        [StringLength(100)]
        public string password { get; set; }

        public int? created_at { get; set; }

        public int? modified_at { get; set; }

        public users()
        {
            Models.psbDBModel databaseInstance = new Models.psbDBModel();
            this.db = databaseInstance;
            this.dh = new DateHelper();
        }

        public List<users> getAllUsers()
        {
            return db.users.Where(a => a.id != 0).ToList();
        }

        public Models.users getUserById(int userIdentifier)
        {
            return this.db.users.Find(userIdentifier);
        }

        public void addUser(string nameValue, string usernameValue, string passwordValue)
        {

            this.db.users.Add(new Models.users
            {
                name = nameValue,
                username = usernameValue,
                password = passwordValue,
                created_at = this.dh.getCurrentTime()
            });

            this.db.SaveChanges();
        }

        public void udateUser(int userIdentifier, string nameValue, string usernameValue, string passwordValue)
        {
            var userData = this.db.users.Find(userIdentifier);

            if (userData != null)
            {
                userData.name = nameValue;
                userData.username = usernameValue;
                userData.password = passwordValue;
                userData.modified_at = dh.getCurrentTime();

                this.db.SaveChanges();
            }
        }

        public void removeUser(int userIdentifier)
        {
            var userData = this.db.users.Find(userIdentifier);

            if (userData != null)
            {
                this.db.users.Remove(userData);
                this.db.SaveChanges();
            }
        }

        public users getUserLogin(string usernameValue, string passwordValue)
        {
            var userData = this.db.users.Where(a => a.username == usernameValue && a.password == passwordValue).FirstOrDefault();
            return userData;
        }
    }
}
