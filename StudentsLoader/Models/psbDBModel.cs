namespace StudentsLoader.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class psbDBModel : DbContext
    {
        public psbDBModel()
            : base("name=psbDBModel")
        {
        }

        public virtual DbSet<groups> groups { get; set; }
        public virtual DbSet<students> students { get; set; }
        public virtual DbSet<users> users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<students>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<students>()
                .Property(e => e.email)
                .IsUnicode(false);
        }
    }
}
