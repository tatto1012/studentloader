namespace StudentsLoader.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using StudentsLoader.Helpers;
    using System.Linq;

    [Table("projectsecondbreak.groups")]
    public partial class groups
    {
        private Models.psbDBModel db;
        private DateHelper dh;

        public int id { get; set; }

        public int? code { get; set; }

        public int created_at { get; set; }

        public int modified_at { get; set; }

        public groups()
        {
            Models.psbDBModel databaseInstance = new Models.psbDBModel();
            this.db = databaseInstance;
            this.dh = new DateHelper();
        }

        public List<groups> getAllGroups()
        {
            return db.groups.Where(a => a.id != 0).ToList();
        }

        public Models.groups getGroupById(int groupIdentifier)
        {
            return this.db.groups.Find(groupIdentifier);
        }

        public void addGroup(int codeValue)
        {

            this.db.groups.Add(new Models.groups
            {
                code = codeValue,
                created_at = this.dh.getCurrentTime()
            });

            this.db.SaveChanges();
        }

        public void udateGroup(int groupIdentifier, int codeValue)
        {
            var groupData = this.db.groups.Find(groupIdentifier);

            if (groupData != null)
            {
                groupData.code = codeValue;
                groupData.modified_at = dh.getCurrentTime();

                this.db.SaveChanges();
            }
        }

        public void removeGroup(int groupIdentifier)
        {
            var groupData = this.db.groups.Find(groupIdentifier);

            if (groupData != null)
            {
                this.db.groups.Remove(groupData);
                this.db.SaveChanges();
            }
        }
    }
}
