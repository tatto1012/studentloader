-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-10-2016 a las 23:20:23
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `projectsecondbreak`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `modified_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `code`, `created_at`, `modified_at`) VALUES
(1, 800, 1476685338, 0),
(2, 801, 1476685356, 0),
(4, 803, 1476685811, 0),
(5, 807, 1476688870, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` double DEFAULT '0',
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `group` int(11) DEFAULT '0',
  `score` varchar(3) NOT NULL DEFAULT '0',
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT '0',
  `modified_at` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`id`, `code`, `name`, `email`, `group`, `score`, `notified`, `created_at`, `modified_at`) VALUES
(1, 2016101713303, 'Alejandra Candela', 'tatto1012+aleja@gmail.com', 800, '3.1', 1, 1476728827, 1476825522),
(2, 2016101713278, 'eva lozano', 'tatto1012+eva@gmail.com', 800, '1.9', 1, 1476728827, 1476825522),
(3, 2016101713352, 'Jonathan Rodriguez', NULL, 800, '2.8', 0, 1476728827, 1476825522),
(4, 2016101713363, 'Andres Suarez', 'tatto1012+andres@gmail.com', 800, '4', 0, 1476728827, 1476825522),
(5, 2016101713318, 'Brandon Silva', 'tatto1012+brandon@gmail.com', 800, '3.5', 0, 1476728827, 1476825522);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` int(11) DEFAULT '0',
  `modified_at` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `created_at`, `modified_at`) VALUES
(1, 'Administrator', 'admin', 'admin', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
