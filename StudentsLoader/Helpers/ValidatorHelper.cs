﻿using Newtonsoft.Json.Linq;
using StudentsLoader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class ValidatorHelper
    {
        private string emailExpression;
        private JObject errors;

        public ValidatorHelper()
        {
            this.errors = new JObject();
            this.emailExpression = @"^[a-z]+[a-z0-9+._]+@[a-z]+\.[a-z.]{2,5}$";
        }

        public bool IsValidEmailFormat(string email)
        {
            if (!this.IsEmptyValue(email))
            {
                Regex rgx = new Regex(this.emailExpression);
                return rgx.IsMatch(email);
            }
            else {
                return false;
            } 
        }

        public bool IsEmptyValue(string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public bool IsValidGroupReference(int groupReference) {
            groups g = new groups();
            var allGroups = g.getAllGroups().ToList();
            var groupValidation = false;

            if (groupReference == 0)
            {
                return true;
            }
            else
            {
                foreach (groups groupItem in allGroups)
                {
                    if (groupItem.code == groupReference)
                    {
                        groupValidation = true;
                        break;
                    }
                }

                return groupValidation;
            }            
        }

        public bool IsValidStudentRecord(int rowNumber, double codeValue, string name, string email, int groupValue)
        {
            var row = rowNumber.ToString();
            if (this.IsEmptyValue(codeValue.ToString()) || this.IsEmptyValue(name))
            {
                this.errors["StudentName" + row] = "Not allow empty value in the colum ´StudentName´ row " + row;
                return false;
            }
            else if (this.IsEmptyValue(email))
            {
                this.errors["StudentEmail" + row] = "Not allow empty value in the colum ´StudentEmail´ row " + row;
                return false;
            }
            else if (this.IsEmptyValue(groupValue.ToString()))
            {
                this.errors["StudentGroup" + row] = "Not allow empty value in the colum ´StudentGroup´ row " + row;
                return false;
            }
            else if (!this.IsValidEmailFormat(email))
            {
                this.errors["StudentEmail" + row] = "Invalid email format in the colum ´StudentEmail´ row "+ row;
                return false;
            }
            else if (!this.IsValidGroupReference(groupValue))
            {
                this.errors["StudentGroup" + row] = "Invalid group reference in the colum ´StudentGroup´ row " + row;
                return false;
            }
            else if (!this.isUniqueStudent(name))
            {
                this.errors["StudentName" + row] = "studentName already exists in the colum ´StudentName´ row " + row;
                return false;
            }
            else
            {
                this.errors = new JObject();
                return true;
            }
        }

        public bool isUniqueStudent(string name)
        {
            var isUniqueStudet = false;
            students student_model = new students();
            var studentData = student_model.getStudentByName(name);
            if (studentData == null) {
                isUniqueStudet = true;
            }
            return isUniqueStudet;
        }

        public JObject getErrors()
        {
            return this.errors;
        }
    }
}