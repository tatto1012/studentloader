﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class GroupStudentsScoreHelper
    {
        public string score { get; set; }

        public bool sendEmail { get; set; }

        public int studentIdentifier { get; set; }
    }
}