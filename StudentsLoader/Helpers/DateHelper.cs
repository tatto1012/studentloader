﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class DateHelper
    {
        public Int32 getCurrentTime()
        {
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp;
        }

        public int getCurrentYear()
        {
            return DateTime.Now.Year;
        }

        public int getCurrentMonth()
        {
            return DateTime.Now.Month;
        }

        public int getCurrentDay()
        {
            return DateTime.Now.Day;
        }

        public int getCurrentTimeReference()
        {
            var hour = DateTime.Now.Hour;
            var minute = DateTime.Now.Minute;
            var second = DateTime.Now.Second;

            return int.Parse(hour.ToString() + minute.ToString() + second.ToString());
        }
    }
}