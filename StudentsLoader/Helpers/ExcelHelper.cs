﻿using StudentsLoader.Models;
using System;
using System.Data;
using System.Data.OleDb;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class ExcelHelper
    {
        private HttpPostedFileBase _file;
        private string _fileLocation;
        private string _fileExtension;
        private string _excelConnectionString;
        private OleDbConnection _excelConnection;
        private DataTable _dt;
        private DataSet _ds;
        private JsonResponse _excelResponse;

        public ExcelHelper(HttpPostedFileBase File, string serverMap)
        {
            this._excelResponse = new JsonResponse();
            this._file = File;
            this.SetFileExtension(File);
            this.SetFileLocation(File, serverMap);
            if (this.FileAlreadyExists())
            {
                this.RemoveFile();
            }
            this.SaveFileInContentTmp();
            this.generateExcelConnection();
        }

        public string GetFileLocation()
        {
            return _fileLocation;
        }

        public string GetFileExtension()
        {
            return _fileExtension;
        }

        private void SetFileExtension(HttpPostedFileBase file)
        {
            this._fileExtension = System.IO.Path.GetExtension(file.FileName);
        }

        private void SetFileLocation(HttpPostedFileBase file, string serverMap)
        {
            this._fileLocation = serverMap + file.FileName;
        }

        public bool HasValidEstension()
        {
            bool isValidEstension = false;

            switch (this._fileExtension)
            {
                case ".xls":
                    isValidEstension = true;
                    break;
                case ".xlsx":
                    isValidEstension = true;
                    break;
            }
            return isValidEstension;
        }

        public bool FileAlreadyExists()
        {
            return System.IO.File.Exists(this._fileLocation);
        }

        public void RemoveFile()
        {
            System.IO.File.Delete(this._fileLocation);
        }

        public void SaveFileInContentTmp()
        {
            this._file.SaveAs(this._fileLocation);
        }

        public string GetExcelStringConection()
        {
            var excelStringConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this._fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\""; ;

            switch (this.GetFileExtension())
            {
                case ".xls":
                    excelStringConnection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this._fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    break;
                case ".xlsx":
                    excelStringConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this._fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    break;
            }

            return excelStringConnection;
        }

        public void generateExcelConnection()
        {
            this._excelConnectionString = this.GetExcelStringConection();
            //Create Connection to Excel work book and add oledb namespace
            this._excelConnection = new OleDbConnection(this._excelConnectionString);
            this._excelConnection.Open();
            this._dt = new DataTable();
            this._dt = this._excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (this._dt == null)
            {
                this._excelResponse.setJsonResponse("error", false, "Unable to read the empty file");
            }
            else
            {
                this._ds = new DataSet();
                String[] excelSheets = new String[this._dt.Rows.Count];
                int t = 0;
                //excel data saves in temp file here.
                foreach (DataRow row in this._dt.Rows)
                {
                    excelSheets[t] = row["TABLE_NAME"].ToString();
                    t++;
                }
                OleDbConnection excelConnection1 = new OleDbConnection(this._excelConnectionString);

                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                {
                    dataAdapter.Fill(this._ds);
                }

                this.processExcelFile();
            }
        }

        public void processExcelFile()
        {
            var vd = new ValidatorHelper();
            students students_model = new students();
            bool excelHasDataError = false;

            for (int i = 0; i < this._ds.Tables[0].Rows.Count; i++)
            {
                var codeValue = new CodeGenerator().getNewCode();
                var nameValue = this._ds.Tables[0].Rows[i][0].ToString().Trim();
                var emailValue = this._ds.Tables[0].Rows[i][1].ToString().Trim();
                var groupValue = 0;
                try
                {
                    groupValue = int.Parse(this._ds.Tables[0].Rows[i][2].ToString());
                }
                catch (Exception e)
                {
                    e.ToString();
                }

                if (vd.IsValidStudentRecord(i, codeValue, nameValue, emailValue, groupValue))
                {
                    students_model.addStudent(codeValue, nameValue, emailValue, groupValue);
                }
                else
                {
                    excelHasDataError = true;
                }
            }

            if (excelHasDataError) {
                this._excelResponse.setJsonResponse("error", false, "The file was proccesed but skip some records");
                this._excelResponse.setDataResponse(vd.getErrors());
            }
            this._excelConnection.Close();
        }

        public JsonResponse getProcessResponse() {
            return this._excelResponse;
        }
    }
}