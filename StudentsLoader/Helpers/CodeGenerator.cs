﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentsLoader.Helpers;


namespace StudentsLoader.Helpers
{
    public class CodeGenerator
    {
        public double getNewCode()
        {
            var dh = new DateHelper();
            var year = dh.getCurrentYear();
            var month = dh.getCurrentMonth();
            var day = dh.getCurrentDay();
            var time = dh.getCurrentTimeReference();

            return double.Parse(year.ToString() + month.ToString() + day.ToString() + time.ToString()) + this.getRandomNumber();
        }

        public int getRandomNumber()
        {
            Random rnd = new Random();
            return rnd.Next(1, 100);
        }
    }
}