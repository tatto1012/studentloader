﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class JsonResponse
    {
        public JObject jsonResult;

        public JsonResponse()
        {
            this.jsonResult = new JObject();

            this.jsonResult["reason"] = "success";//[sucess, error];
            this.jsonResult["result"] = true;
            this.jsonResult["message"] = "You don't set the result message yet!";
        }

        public void setJsonResponse(string status, bool result, string message)
        {
            this.jsonResult["reason"] = status;
            this.jsonResult["result"] = result;
            this.jsonResult["message"] = message;
        }

        public void setDataResponse(JObject data)
        {
            this.jsonResult["data"] = data;
        }

        public string getStringResponse()
        {
            return this.jsonResult.ToString();
        }

        public JObject getJsonResult() {
            return this.jsonResult;
        }
    }
}