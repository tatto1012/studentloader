﻿using Mandrill;
using Mandrill.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsLoader.Helpers
{
    public class MandrillHelper
    {

        public MandrillApi api;
        public MandrillMessage message;

        public MandrillHelper()
        {
            this.api = new MandrillApi("xZZdgbQdNKvJxYzDibo-WQ");
            this.message = new MandrillMessage();
        }

        public void sendMessage(string emailRecipient, string groupReference, string studentName, string studentscore)
        {
            message.FromEmail = "no-reply@uninpahu.com";
            message.AddTo(emailRecipient);
            message.ReplyTo = "customerservice@uninpahu.com";

            //supports merge var content as string
            message.AddGlobalMergeVars("groupReference", groupReference);
            message.AddGlobalMergeVars("studentName", studentName);
            message.AddGlobalMergeVars("studentScore", studentscore);

            var result = api.Messages.SendTemplateAsync(message, "studentScoreNotification");
        }
    }
}