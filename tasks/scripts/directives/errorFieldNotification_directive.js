app.directive( 'errorFieldNotification', function(_config, appHelper){
  return {
    restrict: 'E',
    templateUrl: appHelper.templatePath('error-field-notification'),
    scope:{
      fieldToValidate: '=fieldToValidate',
      minlength: '=minlength',
      maxlength: '=maxlength',
    }
  };
});