app.controller('groupDetailCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {

	$scope.notifyAllStudents = false;

	$scope.initializeActions = function(studentIdentifier, hasEmail, score) {
		$scope["studentSendEmailDisabled_" + studentIdentifier] = (_.isEmpty(hasEmail))? true : false;
		$scope["studentSendEmailChecked_" + studentIdentifier] = false;
		$scope["studentScore_" + studentIdentifier] = score;
	}

	$scope.toggleAll = function() {
		var $rowItems = $(".student-row"),
			studentIdentifier;

		if($scope.notifyAllStudents) {
			_.each($rowItems, function(studentItem) {
				studentIdentifier = $(studentItem).data('identifier');
				if(!$scope["studentSendEmailDisabled_" + studentIdentifier]) {
					$scope["studentSendEmailChecked_" + studentIdentifier] = true;
				}
			});
		} else {
			_.each($rowItems, function(studentItem) {
				studentIdentifier = $(studentItem).data('identifier');
				$scope["studentSendEmailChecked_" + studentIdentifier] = false;
			});
		}
	};

	$scope.goGroupList = function(wait) {
		$rootScope.redirecTo('Group/Index', wait);
	}

	$scope.updateStudentsNotes = function() {
		var $rowItems = $(".student-row"),
			student,
			studentIdentifier;

		$scope.studentsData = [];

		_.each($rowItems, function(studentItem) {
			studentIdentifier = $(studentItem).data('identifier');
			student = {
				"studentIdentifier": studentIdentifier,
				"score": $scope['studentScore_' + studentIdentifier] + "",
				"sendEmail": $scope["studentSendEmailChecked_" + studentIdentifier]
			};
			$scope.studentsData.push(student);
		});

		var requestData = {
			"studentsScoreData": JSON.stringify($scope.studentsData)
		};

		APIRequestService.updateGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$scope.goGroupList(true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};

}]);