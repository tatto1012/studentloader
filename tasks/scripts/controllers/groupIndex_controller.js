app.controller('groupIndexCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {


	$scope.detailGroup = function(groupIdentifier) {
		$rootScope.redirecTo('Group/Detail/' + groupIdentifier, false);
	};

	$scope.removeGroup = function(groupIdentifier) {
		$scope["showWarning_" + groupIdentifier] = true;
	};

	$scope.cancelRemoveActionFor = function(groupIdentifier) {
		$scope["showWarning_" + groupIdentifier] = false;
	};

	$scope.confirmRemoveActionFor = function(groupIdentifier) {
		var requestData = {
			"groupIdentifier": groupIdentifier
		};

		APIRequestService.deleteGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Group/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};

	$scope.createGroup = function() {
		var requestData= {};

		APIRequestService.createGroup(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Group/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	}
}]);