app.controller('loginCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr){
	var formFields = ['username', 'password'];

	$scope.singUp = function(formData){
		if(formData.$valid){
			var requestData = {
				'username': $scope.formUsername,
				'password': $scope.formPassword
			};

			APIRequestService.singin(requestData, function(error, response){
				if(!error && response &&  response.data && response.data.reason == 'success'){
					toastr.success(response.data.message, "Success");
					$rootScope.redirecTo('Student/Index', true);
				} else if (!error && response.data.reason == 'error') {
					toastr.error(response.data.message, "Error");
				} else {
					toastr.error($rootScope.genericErrorMessage, "Error");
				}
			});
		}
	};

	$scope.showFieldErrors = function(formData, errors){
		_.each(formFields, function(field){
			formData[field].$error.invalid = true;
			formData[field].$error_message = errors[field];
		});
	};

}]);