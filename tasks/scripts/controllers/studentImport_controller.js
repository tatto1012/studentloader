app.controller('studentImportCtrl', [
	'_config',
	'_apiEndpointsList',
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
	'FileUploader',
function(_config, _apiEndpointsList, $rootScope, $scope, APIRequestService, toastr, FileUploader){
	$scope.uploader = new FileUploader({
        url: _apiEndpointsList.importStudents
    });

	$scope.file = {
		progressPositive: 5,
		progressNegative: 95
	};
    $scope.showFileInput = false;
    $scope.showFileDescription = false;
    $scope.disabledImportAction = true;
    $scope.showProgressbar = false;
    $scope.showskipedExcelRecords = false;

    $scope.getExcelFormat = function() {
    	window.open(_config.excelStudentLoaderFile);
    };

    $scope.triggerUploadEvent = function() {
    	$("input[type='file']").trigger('click');
    }

	$scope.goStudentsList = function(wait) {
		$rootScope.redirecTo('Student/Index', wait);
	}

	$scope.getTypeOfFile = function(fileReference) {
		var data = {
			icon: "fa-question",
			type: "Unknow"
		};

		switch(fileReference) {
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				data.type = 'Excel';
				data.icon = 'fa-file-excel-o';
				break;
			case "application/msword":
				data.type = 'Word';
				data.icon = 'fa-file-word-o';
				break;
			case "image/jpeg":
			case "image/png":
			case "image/gif":
				data.type = 'Image';
				data.icon = 'fa-file-image-o';
				break;
			case "application/pdf":
				data.type = 'Pdf';
				data.icon = 'fa-file-pdf-o';
				break;
		}

		return data;
	}

    $scope.uploader.onWhenAddingFileFailed = function(item, filter, options) {
    	$scope.showFileDescription = false;
        toastr.error("Something when wrong", "Error");
    };

    $scope.uploader.onAfterAddingFile = function(fileItem) {
    	var fileInfo = $scope.getTypeOfFile(fileItem.file.type);
    	
    	$scope.showFileDescription = true;
    	$scope.file['name'] = fileItem.file.name;
    	$scope.file['size'] = fileItem.file.size;
    	$scope.file['lastModifiedDate'] = moment(fileItem.file.lastModifiedDate.getTime()).format();
    	$scope.file['type'] = fileInfo.type;
    	$scope.file['icon'] = fileInfo.icon;
    };
    
    $scope.uploader.onBeforeUploadItem = function(item) {
    	$scope.showProgressbar = true;
        $scope.showskipedExcelRecords = false;
    };

    $scope.uploader.onProgressItem = function(fileItem, progress) {
    	$scope.file.progressPositive = progress;
    	$scope.file.progressNegative = (100 - progress);
    };

    $scope.uploader.onProgressAll = function(progress) {
    	$scope.file.progressPositive = progress;
    	$scope.file.progressNegative = (100 - progress);
    };

    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
    	if (response && response.reason === "success") {
            if (response.data) {
                $scope.showSkipedRecords(response.data);
                toastr.warning(response.message, "Warning");
                $scope.showProgressbar = false;
            } else {
        		toastr.success(response.message, "Success");
        		$scope.goStudentsList(true);
            }
    	} else if(response && response.reason === "error") {
    		$scope.showProgressbar = false;
    		toastr.error(response.message, "Error");
    	} else {
    		$scope.showProgressbar = false;
    		toastr.error("Something when wrong, try again", "Error");
    	}
    };

    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
    	console.log(response);
    	toastr.error("Something when wrong", "Error");
    };

    $scope.showSkipedRecords = function(records) {
        var $skippedList = $("#skipedExcelRecords");
        $skippedList.empty();
        _.each(records, function(value, key){
            $skippedList.append("<li>"+value+"</li>");
        });
        $scope.showskipedExcelRecords = true;
    };

}]);