app.controller('studentIndexCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr) {

	$scope.uploadExcelFile = function() {
		$rootScope.redirecTo('Student/Import', false);
	};

	$scope.createUser = function() {
		$rootScope.redirecTo('Student/Add', false);
	};

	$scope.editStudent = function(studentIdentifier) {
		$rootScope.redirecTo('Student/Edit/'+ studentIdentifier, false);
	};

	$scope.removeStudent = function(studentIdentifier) {
		$scope["showWarning_" + studentIdentifier] = true;
	};

	$scope.cancelRemoveActionFor = function(studentIdentifier) {
		$scope["showWarning_" + studentIdentifier] = false;
	};

	$scope.confirmRemoveActionFor = function(studentIdentifier) {
		var requestData = {
			"studentIdentifier": studentIdentifier
		};

		APIRequestService.deleteStudent(requestData, function(error, response){
			if(!error && response &&  response.data && response.data.reason == 'success'){
				toastr.success(response.data.message, "Success");
				$rootScope.redirecTo('Student/Index', true);
			} else if (!error && response.data.reason == 'error') {
				toastr.error(response.data.message, "Error");
			} else {
				toastr.error($rootScope.genericErrorMessage, "Error");
			}
		});
	};
}]);