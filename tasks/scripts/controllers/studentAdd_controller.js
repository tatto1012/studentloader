app.controller('studentAddCtrl', [
	'$rootScope',
	'$scope',
	'APIRequestService',
	'toastr',
function($rootScope, $scope, APIRequestService, toastr){

	$scope.goStudentsList = function(wait) {
		$rootScope.redirecTo('Student/Index', wait);
	}

	$scope.createStudent = function(formData) {
		if(formData.$valid) {
			var requestData = {
				"code": $scope.studentCode,
				"name": $scope.studentName,
				"email": $scope.studentEmail,
				"group": $scope.studentGroup
			};

			APIRequestService.createStudent(requestData, function(error, response){
				if(!error && response &&  response.data && response.data.reason == 'success'){
					toastr.success(response.data.message, "Success");
					$scope.goStudentsList(true);
				} else if (!error && response.data.reason == 'error') {
					toastr.error(response.data.message, "Error");
				} else {
					toastr.error($rootScope.genericErrorMessage, "Error");
				}
			});
		}
	};

}]);