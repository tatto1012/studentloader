app.constant('_config', {
	'appRefenrece': 'StudentLoader',
	'defaultTimeout': 3000,
	'baseUrl': "http://localhost:3000/",
	'requestTimeout': 3000,
	'genericErrorMessage':  "Internal Server Error has ocurr, try agein",
	'excelStudentLoaderFile': "Content/Formats/studentLoaderFormat_v1.xlsx"
});

app.constant('_apiEndpointsList', {
	'signin': 'Login/SignIn',
	'createStudent': 'Student/AddStudent',
	'updateStudent': 'Student/UpdateStudent',
	'deleteStudent': 'Student/DeleteStudent',
	'importStudents': 'Student/ImportStudents',
	'createGroup': 'Group/AddGroup',
	'deleteGroup': 'Group/DeleteGroup',
	'updateGroup': 'Group/UpdateStudentsGroup'
});

app.constant('_regExp', {
	'onlyNumbersPattern': /^[0-9.]+$/,
	'onlyAlphabethPattern': /^[a-z _-]+$/,
	'emailPattern': /^[a-z]+[a-z0-9+._]+@[a-z]+\.[a-z.]{2,5}$/
});