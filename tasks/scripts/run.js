app.run([
    '_config',
    '_regExp',
    '$rootScope',
    '$timeout',
    '$window',
function(_config, _regExp, $rootScope, $timeout, $window) {
    _.extend($rootScope, _regExp);
    _.extend($rootScope, _config);

    $rootScope.redirecTo = function(urlRedirection, timeWaiting){
    	timeWaiting = (timeWaiting)? _config.defaultTimeout : 0;
    	$timeout(function() {
    		$window.location.href = _config.baseUrl + urlRedirection;
    	}, timeWaiting);
    };

    $('span').tooltip();
}]);