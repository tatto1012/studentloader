app.factory('APIRequestService', [
  '_config',
  '_apiEndpointsList',
  '$http',
function (_config, _apiEndpointsList, $http) {
    var APIRequestService = this;

    APIRequestService.madeRequestAction = function(method, endpointReference, requestData, addTokenToRequest, callback) {
      //define the http options
      var httpOptions = {
          method : method,
          url    : _config.baseUrl + endpointReference,
          data   : requestData,
          cache  : false,
          timeout: _config.requestTimeout
      };

      $http(httpOptions).then(function successCallback(response) {
    		callback(null, response);
      }, function errorCallback(response) {
      	callback(response.data);
      });
    };

    APIRequestService.singin = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST',_apiEndpointsList.signin, requestData,  false, callback);
    };

    APIRequestService.createStudent = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.createStudent, requestData,  false, callback);
    };

    APIRequestService.updateStudent = function (requestData, callback) {
    	APIRequestService.madeRequestAction('POST', _apiEndpointsList.updateStudent, requestData,  false, callback);
    };

    APIRequestService.deleteStudent = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.deleteStudent, requestData,  false, callback);
    };

    APIRequestService.importStudents = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.importStudents, requestData,  false, callback);
    };

    APIRequestService.createGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.createGroup, requestData,  false, callback);
    };

    APIRequestService.deleteGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.deleteGroup, requestData,  false, callback);
    };

    APIRequestService.updateGroup = function (requestData, callback) {
      APIRequestService.madeRequestAction('POST', _apiEndpointsList.updateGroup, requestData,  false, callback);
    };

    return APIRequestService;
}]);