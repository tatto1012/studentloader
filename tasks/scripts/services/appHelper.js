app.constant('appHelper', {

  templatesDir: function() {
  	return 'http://localhost:3000/Content/templates';
  },

  templatePath: function(view_name){
    return this.templatesDir() + '/' + view_name + '.html';
  }

});