var j_core_configuration = require('./gulpConfig.js');

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    tap = require('gulp-tap'),
    minifyCss = require('gulp-minify-css'),
    open = require("open"),
    livereload = require('gulp-livereload'),
    gulpCopy = require('gulp-copy'),
    mkdirp = require('mkdirp'),
    replace = require('gulp-replace');

function getFileLoc(file){
  return require('path').relative( './', file.path);
}

gulp.task('default', ['prepareAssests:copy', 'sass:app', 'sass:vendor', 'js:app', 'js:vendor', 'watch']);

gulp.task('prepareAssests:copy', ['copyJSAssets', 'copyTemplatesAssets', 'copyFontsIconsAssets']);

gulp.task('clean', function(){
  return gulp.src( j_core_configuration.gulpTask.cleanFilesList, { read:false })
  .pipe(clean({force: true}));
});

gulp.task('copyJSAssets', function(){
  return gulp.src(j_core_configuration.gulpTask.copyJSAssetsFiles.files)
  .pipe(gulpCopy(j_core_configuration.gulpTask.copyJSAssetsFiles.outputPath, j_core_configuration.gulpTask.copyJSAssetsFiles.options));
});

gulp.task('copyTemplatesAssets', function(){
  return gulp.src(j_core_configuration.gulpTask.copyTemplatesAssets.files)
  .pipe(gulpCopy(j_core_configuration.gulpTask.copyTemplatesAssets.outputPath, j_core_configuration.gulpTask.copyTemplatesAssets.options));
});

gulp.task('copyFontsIconsAssets', function(){
  return gulp.src(j_core_configuration.gulpTask.copyFontsIconsAssets.files)
  .pipe(gulpCopy(j_core_configuration.gulpTask.copyFontsIconsAssets.outputPath, j_core_configuration.gulpTask.copyFontsIconsAssets.options));
});

gulp.task('sass:app', function(done) {
  gulp.src(j_core_configuration.gulpTask.sassPaths.appStyles)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(rename(j_core_configuration.gulpTask.targetNames.appCss))
    .pipe(gulp.dest(j_core_configuration.gulpTask.buildDirectories.css))
    .on('end', done);
});

gulp.task('sass:vendor', function(done) {
  gulp.src(j_core_configuration.gulpTask.sassPaths.vendor)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(rename(j_core_configuration.gulpTask.targetNames.vendorCss))
    .pipe(gulp.dest(j_core_configuration.gulpTask.buildDirectories.css))
    .on('end', done);
});

gulp.task('js:app', function(){
  return gulp.src( j_core_configuration.gulpTask.appScripts )
  .pipe(tap(function(file){
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( j_core_configuration.gulpTask.targetNames.appJs ))
  .pipe(gulp.dest( j_core_configuration.gulpTask.buildDirectories.js ));
});

gulp.task('js:vendor', function(){
  return gulp.src( j_core_configuration.gulpTask.vendorScripts )
  .pipe(tap(function(file){
    var fileLoc = getFileLoc(file);
    file.contents = Buffer.concat([
      new Buffer('/* ====== ' + fileLoc  + ' ===-_-=== */\n'),
      file.contents
    ]);
  }))
  .pipe(concat( j_core_configuration.gulpTask.targetNames.vendorJs ))
  .pipe(gulp.dest( j_core_configuration.gulpTask.buildDirectories.js ));
});

gulp.task('watch', function(){
  gulp.watch(j_core_configuration.gulpTask.appStyles, ['sass:app']);
  gulp.watch(j_core_configuration.gulpTask.vendorStyles, ['sass:vendor']);
  gulp.watch(j_core_configuration.gulpTask.appScripts, ['js:app']);
  gulp.watch(j_core_configuration.gulpTask.vendorScripts, ['js:vendor']);
  gulp.watch(j_core_configuration.gulpTask.templatesAndViews, ['copyTemplatesAssets']);
});