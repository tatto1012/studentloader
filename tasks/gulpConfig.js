var gulpConfiguration = {
    "gulpTask": {
        "buildDirectories": {                                                
            "js": "../StudentsLoader/Scripts/assets/",
            "css": "../StudentsLoader/Content/assets/"
        },
        "targetNames": {                                                    
            "appJs": "app.js",
            "vendorJs": "vendor.js",
            "appCss": "styles.css",
            "vendorCss": "vendor.css"
        },
        "sassPaths" : {                                                      
            'appStyles': './styles/appStyles.scss',
            'vendor': './styles/vendorStyles.scss'
        },
        "appStyles":[                                                        
            "./styles/*.scss",
            "./styles/**/*.scss",
            "./styles/**/**/*.scss",
        ],
        "vendorStyles":[                                                     
            "./node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss",
            "./node_modules/font-awesome/scss/font-awesome.scss",
            "./node_modules/angular-toastr/dist/angular-toastr.min.css"
        ],
        "appScripts": [                                                      
            "./scripts/app.js",
            "./scripts/run.js",
            "./scripts/config.js",
            "./scripts/constants.js",
            "./scripts/controllers/*",
            "./scripts/controllers/**/*",
            "./scripts/directives/*",
            "./scripts/directives/**/*",
            "./scripts/filters/*",
            "./scripts/filters/**/*",
            "./scripts/services/*",
            "./scripts/services/**/*"
        ],
        "vendorScripts": [
            "./node_modules/jquery/dist/jquery.min.js",
            "./node_modules/angular/angular.min.js",
            "./node_modules/moment/moment.js",
            "./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js",
            "./node_modules/lodash/lodash.min.js",
            "./node_modules/angular-animate/angular-animate.min.js",
            "./node_modules/angular-toastr/dist/angular-toastr.min.js",
            "./node_modules/angular-toastr/dist/angular-toastr.tpls.min.js",
            "./node_modules/angular-file-upload/dist/angular-file-upload.min.js"
        ],
        "cleanFilesList": [                                                  
            '../StudentsLoader/Scripts/assets/*',
            '../StudentsLoader/Content/assets/*',
            '../StudentsLoader/fonts/*'
        ],
        "copyJSAssetsFiles": {                                              
            "outputPath": "../StudentsLoader/Scripts/assets/",
            "options": {
                "prefix": 3
            },
            "files": [
                './node_modules/angular/angular.min.js.map',
                './node_modules/jquery/dist/jquery.min.map',
                "./node_modules/angular-animate/angular-animate.min.js.map",
                "./node_modules/angular-file-upload/dist/angular-file-upload.min.js.map"
            ]
        },
        "copyTemplatesAssets": {                                             
            "outputPath": "../StudentsLoader/Content/templates/",
            "options": {
                "prefix": 4
            },
            "files": [
                'templates/*.html'
            ]
        },
        "copyFontsIconsAssets": {                                                     
            "outputPath": "../StudentsLoader/fonts/",
            "options": {
                "prefix": 6
            },
            "files": [
                './node_modules/bootstrap-sass/assets/fonts/bootstrap/*.*',
                './node_modules/font-awesome/fonts/*.*'
            ]
        },
        "templatesAndViews": [
            "templates/*.html",
            "templates/**/*.html"
        ]
    }
};

module.exports = gulpConfiguration;
